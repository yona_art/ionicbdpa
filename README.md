# Project DBPA

### Integrantes
Andres Ayala Franco<br>
Alexis David Moreal Gonzales<br>
Oscar Arturo de luna lujan<br>
Julio Cesar Picazo <br>
Juan Daniel Ramirez Torres<br>
Miguel Angel Marin Rivera<br>
Yonatan Asael Garcia Ortiz<br>

## Backend
El backend se encuentra en otro repositorio ->
[Click Aqui](https://gitlab.com/yona_art/ionbe)


## Transacciones
La transacciones se encuentran alojada en este link ->
[Transacciones Archivo](https://gitlab.com/yona_art/ionbe/-/blob/master/src/main/java/com/ion/spb/Services/RequestService.java)


```java
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public Request updateByStatus(Long id, Long status) {
        Request requested = repository.getOne(id);
        requested.setStatus(status);
        return repository.save(requested);
    }
```

```java
  @Transactional(isolation = Isolation.SERIALIZABLE)
    public Finalized saveFinaled(Request Request, Long id) {
        Request newRequest = repository.save(Request);
        Finalized newFinalized = new Finalized("", userService.getOne(id), newRequest);
        return finalizedService.save(newFinalized);
    }
```

## Esquema de Base de datos
Este es el esquema que tiene el servidor de base de datos de MYSQL, el cual es manipulado por un backend SpringBoot y JPA.

![](files/DB.png)

## Ejemplo de funcionamiento

### Register

Para poder ingresar al sistema necesitas crear una cuenta ya que esta aplicacion es planeada unicamente con registro de telefono,
pero como en este caso no contamos con el servicio el registro es ilimitado, sin verificacion.

![Register](files/Register.gif)

### Login
Despues de tener una cuenta en el sistema para volver ingresar debemos ingresar el folio o username para obtener un token para ingreso al sistema.

![Login](files/Login.gif)

### Redirect
Esta parte detecta que tipo de plataforma es y este te redireccionara a la aplicacion necesaria para abrir el mapa en especifico

![Redirect](files/Redirect.gif)

### Status
Aquí aplicamos las transacciones del sistema para esta cambiar los estados del item así evitando que se solicite algún cambio en la base de datos por parte de otra persona hasta que no se termine de realizar la consulta. También mostrando los datos a usuarios que lo soliciten hasta que se termine una operación de inserción o actualización de datos.

![Status](files/Status.gif)
