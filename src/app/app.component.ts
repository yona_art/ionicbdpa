import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { AuthService } from './Core/Services/auth.service';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { Router } from '@angular/router';
import { DBTables } from './Core/Enums/DBTables.enums';
import { DriverLocalModel } from './Core/Models/Drivers.LocalModel';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(
    private platform: Platform,
    private authservice: AuthService,
    private router: Router,
  ) { }


  ngOnInit() {
    this.isAutorized();
  }

  isAutorized() {
    this.authservice.loggedIn().subscribe(res => {
      if (res) {
        this.router.navigateByUrl('/List/Items');
      } else {
        this.router.navigateByUrl('/Auth/Login');
      }
    });

  }

}
