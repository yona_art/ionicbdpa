import { UserModel } from './User.model';
import { RequestModel } from './Requests.model';
export class FinalizedModel {

    id: number;
    description: string;
    user: UserModel;
    request: RequestModel;

    constructor() {
        this.setDefaultValues();
    }

    setDefaultValues() {
        this.id = 0;
        this.description = '';
        this.user = new UserModel();
        this.request = new RequestModel();
    }

    setValuesFromObject(Object) {
        this.id = Object.id;
        this.description = Object.description;
        this.user = Object.user;
        this.request = Object.request;
    }

}
