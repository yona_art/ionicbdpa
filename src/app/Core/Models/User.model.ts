export class UserModel {
    public id: number;
    public name: string;
    public folio: string;
    public password: string;
    public phone: string;
    public isWorker: number;

    constructor() {
        this.setDefaultValues();
    }

    setDefaultValues() {
        this.id = 0;
        this.name = '';
        this.folio = '';
        this.password = '';
        this.phone = '';
        this.isWorker = 0;
    }

}
