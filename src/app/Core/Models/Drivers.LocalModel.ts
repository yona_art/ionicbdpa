export class DriverLocalModel {
    id: number;
    folio: string;
    name: string;
    phone: string;

    constructor() {
        this.setDefaultValues();
    }

    setDefaultValues() {
        this.id = 0;
        this.folio = '';
        this.name = '';
        this.phone = '';
    }

    setValuesFromObject(Object) {
        this.id = Object.id;
        this.folio = Object.folio;
        this.name = Object.name;
        this.phone = Object.phone;
        return this;
    }
}
