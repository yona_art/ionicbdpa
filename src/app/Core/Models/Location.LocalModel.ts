export class LocationLocalModel {
    id: number;
    status: number;
    description: string;
    driverid: number;
    pointid: number;

    constructor() {
        this.setDefaultValues();
    }

    setDefaultValues() {
        this.id = 0;
        this.status = 0;
        this.description = '';
        this.driverid = 0;
        this.pointid = 0;
    }
}
