import { UserModel } from './User.model';
export class RequestModel {
    id: number;
    latitude: string;
    longitude: string;
    status: number;
    description: string;
    user: UserModel;

    constructor() {
        this.setDefaultValue();
    }

    setDefaultValue() {
        this.id = 0;
        this.latitude = '';
        this.longitude = '';
        this.status = 0;
        this.description = '';
        this.user = new UserModel();
    }

    setValuesFromObject(Object) {
        this.id = Object.id;
        this.latitude = Object.latitude;
        this.longitude = Object.longitude;
        this.status = Object.status;
        this.description = Object.description;
        this.user = Object.user;
    }
}
