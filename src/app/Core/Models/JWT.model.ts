import { UserModel } from './User.model';
export class JWTModel {

    public token: string;
    public user: UserModel;


    constructor() {
        this.setDefaultValues();
    }

    setDefaultValues() {
        this.token = '';
        this.user = new UserModel();
    }

}
