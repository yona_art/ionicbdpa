export class PointsLocalModel {
    id: number;
    latitude: string;
    longitude: string;
    description: string;
    userid: number;
    status: number;

    constructor() {
        this.setDefaultValues();
    }

    setDefaultValues() {
        this.id = 0;
        this.latitude = '';
        this.longitude = '';
        this.description = '';
        this.userid = 0;
        this.status = 0;
    }

    setValuesFromObject(Object) {
        this.id = Object.id;
        this.latitude = Object.latitude;
        this.longitude = Object.longitude;
        this.description = Object.description;
        this.userid = Object.userid;
        this.status = Object.status;
    }

    setValuesFromObjectReturnValue(Object) {
        this.id = Object.id;
        this.latitude = Object.latitude;
        this.longitude = Object.longitude;
        this.description = Object.description;
        this.userid = Object.userid;
        this.status = Object.status;
        return this;
    }
}
