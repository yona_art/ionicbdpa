import { ObjectStoreMeta } from 'ngx-indexed-db';
import { DBTables } from '../Enums/DBTables.enums';

export const DBJSON: ObjectStoreMeta[] = [
    {
        store: DBTables.Driver,
        storeConfig: { keyPath: 'id', autoIncrement: true },
        storeSchema: [
            { name: 'folio', keypath: 'folio', options: { unique: false } },
            { name: 'name', keypath: 'name', options: { unique: false } },
            { name: 'phone', keypath: 'phone', options: { unique: false } }
        ]
    },
    {
        store: DBTables.Users,
        storeConfig: { keyPath: 'id', autoIncrement: true },
        storeSchema: [
            { name: 'folio', keypath: 'folio', options: { unique: false } },
            { name: 'name', keypath: 'name', options: { unique: false } },
            { name: 'phone', keypath: 'phone', options: { unique: false } }
        ]
    },
    {
        store: DBTables.Points,
        storeConfig: { keyPath: 'id', autoIncrement: true },
        storeSchema: [
            { name: 'latitude', keypath: 'latitude', options: { unique: false } },
            { name: 'longitude', keypath: 'longitude', options: { unique: false } },
            { name: 'description', keypath: 'description', options: { unique: false } },
            { name: 'status', keypath: 'status', options: { unique: false } },
            { name: 'user_id', keypath: 'user_id', options: { unique: false } },
        ]
    },
    {
        store: DBTables.Locations,
        storeConfig: { keyPath: 'id', autoIncrement: true },
        storeSchema: [
            { name: 'status', keypath: 'status', options: { unique: false } },
            { name: 'description', keypath: 'description', options: { unique: false } },
            { name: 'phone', keypath: 'phone', options: { unique: false } },
            { name: 'driver_id', keypath: 'driver_id', options: { unique: false } },
            { name: 'point_id', keypath: 'point_id', options: { unique: false } },
        ]
    },
];

