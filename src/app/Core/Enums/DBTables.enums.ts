export enum DBTables {
    Driver = 'Drivers',
    Users = 'Users',
    Locations = 'Locations',
    Points  = 'Points',
}
