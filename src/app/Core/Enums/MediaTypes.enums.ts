export enum MediaTypes {
    CONTENT = 'Content-Type',
    JSON = 'application/json',
    XML = 'application/xml',
    TXT = 'text/plain; charset=utf-8',
}
