import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { throwError, Observable, of } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { JWTModel } from '../Models/JWT.model';
import { UserModel } from '../Models/User.model';
import { MediaTypes } from '../Enums/MediaTypes.enums';
import { TokenStorage } from './token-storage.service';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { DriverLocalModel } from '../Models/Drivers.LocalModel';
import { DBTables } from '../Enums/DBTables.enums';
import { StorageEnums } from '../Enums/Storage.enums';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private API = environment.API_ENDPOINT + 'Auth';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': MediaTypes.JSON,
      'Authorization': 'Bearer ' + this.tokenStorage.getAccessToken(),
    })
  };

  constructor(
    private http: HttpClient,
    private tokenStorage: TokenStorage,
    private dbService: NgxIndexedDBService,
    private router: Router,
  ) { }

  public register(user: UserModel): void {
    this.http.post<JWTModel>(this.API + '/Register', user,
      { headers: new HttpHeaders().set('Content-Type', MediaTypes.JSON) }).pipe(
        retry(1),
        catchError(this.handleError),
      ).subscribe(res => {
        this.tokenStorage.clearToken();
        this.saveDriver(res.user);
        this.tokenStorage.setAccessToken(res.token);
      });
  }

  public login(user: UserModel): void {
    this.http.post<JWTModel>(this.API + '/Login', user,
      { headers: new HttpHeaders().set('Content-Type', MediaTypes.JSON) }).pipe(
        retry(1),
        catchError(this.handleError),
      ).subscribe(res => {
        this.saveDriver(res.user);
        this.tokenStorage.clearToken();
        this.tokenStorage.setAccessToken(res.token);
      });
  }

  public loggedIn(): Observable<boolean> {
    return this.http.get<boolean>(this.API + '/', this.httpOptions).pipe(
      catchError(error => {
        return of(false);
      })
    );
  }

  public logout() {
    this.tokenStorage.clearToken();
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = `Error: ${error.error.message}`;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    this.router.navigateByUrl('/Auth/Login');
    return throwError(errorMessage);
  }

  saveDriver(driver: DriverLocalModel) {
    this.tokenStorage.setItems(StorageEnums.UserId, driver.id);
    this.dbService.getAll(DBTables.Driver).then(
      (d) => {
      },
      () => { },
    );
    this.dbService.getByID(DBTables.Driver, driver.id).then(
      (d: DriverLocalModel) => {
        if (!(d.id === driver.id) || d.id === undefined) {
          this.dbService.add(DBTables.Driver, driver.id).then(
            () => { },
            error => {
              console.log(error);
            }
          );
        }
      },
      error => {
        console.log(error);
      }
    );
  }


}
