import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TokenStorage } from './token-storage.service';
import { environment } from '../../../environments/environment';
import { MediaTypes } from '../Enums/MediaTypes.enums';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { RequestModel } from '../Models/Requests.model';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  private API = environment.API_ENDPOINT + 'Requests';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': MediaTypes.JSON,
      'Authorization': 'Bearer ' + this.tokenStorage.getAccessToken(),
    })
  };

  constructor(
    private http: HttpClient,
    private tokenStorage: TokenStorage
  ) { }

  public findOne(id: number): Observable<RequestModel> {
    return this.http.get<RequestModel>(this.API + '/' + id, this.httpOptions).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  public getALL(): Observable<RequestModel[]> {
    return this.http.get<RequestModel[]>(this.API + '/', this.httpOptions).pipe(
      retry(1),
      catchError(this.handleError),
    );
  }
  
  public create(user: RequestModel): Observable<RequestModel> {
    return this.http.post<RequestModel>(this.API, JSON.stringify(user), this.httpOptions).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  public delete(id: number): Observable<boolean> {
    return this.http.delete<boolean>(this.API + `/${id}`, this.httpOptions).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  public update(user: RequestModel): Observable<RequestModel> {
    return this.http.put<RequestModel>(this.API + `/${user.id}`, JSON.stringify(user), this.httpOptions).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  public updateStatus(user: RequestModel, status: number): Observable<RequestModel> {
    return this.http.put<RequestModel>(this.API + `/${user.id}/status/${status}`, JSON.stringify(user), this.httpOptions).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = `Error: ${error.error.message}`;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }

}
