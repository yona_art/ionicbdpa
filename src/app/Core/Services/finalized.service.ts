import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { MediaTypes } from '../Enums/MediaTypes.enums';
import { TokenStorage } from './token-storage.service';
import { throwError, Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { FinalizedModel } from '../Models/Finalized.model';

@Injectable({
  providedIn: 'root'
})
export class FinalizedService {

  private API = environment.API_ENDPOINT + 'Requests';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': MediaTypes.JSON,
      'Authorization': 'Bearer ' + this.tokenStorage.getAccessToken(),
    })
  };

  constructor(
    private http: HttpClient,
    private tokenStorage: TokenStorage
  ) { }

  public findOne(id: number): Observable<FinalizedModel> {
    return this.http.get<FinalizedModel>(this.API + '/' + id, this.httpOptions).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  public getALL(): Observable<FinalizedModel[]> {
    return this.http.get<FinalizedModel[]>(this.API + '/', this.httpOptions).pipe(
      retry(1),
      catchError(this.handleError),
    );
  }

  public create(user: FinalizedModel): Observable<FinalizedModel> {
    return this.http.post<FinalizedModel>(this.API, JSON.stringify(user), this.httpOptions).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  public delete(id: number): Observable<boolean> {
    return this.http.delete<boolean>(this.API + '/' + id, this.httpOptions).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  public update(user: FinalizedModel): Observable<FinalizedModel> {
    return this.http.put<FinalizedModel>(this.API + '/' + user.id, JSON.stringify(user), this.httpOptions).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = `Error: ${error.error.message}`;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }

}
