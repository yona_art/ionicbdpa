import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { UserModel } from '../Models/User.model';
import { MediaTypes } from '../Enums/MediaTypes.enums';
import { TokenStorage } from './token-storage.service';
import { FinalizedModel } from '../Models/Finalized.model';

@Injectable({
    providedIn: 'root'
})
export class UsersService {

    private API = environment.API_ENDPOINT + 'users';
    private httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': MediaTypes.JSON,
            // tslint:disable-next-line: object-literal-key-quotes
            'Authorization': 'Bearer ' + this.tokenStorage.getAccessToken(),
        }),
        params: new HttpParams()
    };


    constructor(
        private http: HttpClient,
        private tokenStorage: TokenStorage
    ) { }

    public findOne(id: number): Observable<UserModel> {
        return this.http.get<UserModel>(this.API + `/${id}`, this.httpOptions).pipe(
            catchError(this.handleError)
        );
    }

    public getALL(): Observable<UserModel[]> {
        return this.http.get<UserModel[]>(this.API + '/', this.httpOptions).pipe(
            retry(1),
            catchError(this.handleError),
        );
    }

    public findRequestByID(id: number, status: number): Observable<FinalizedModel[]> {
        this.httpOptions.params = new HttpParams().set('status', String(status));
        return this.http.get<FinalizedModel[]>(this.API + `/${id}/Requests`, this.httpOptions).pipe(
            retry(1),
            catchError(this.handleError),
        );
    }


    public create(user: UserModel): Observable<UserModel> {
        return this.http.post<UserModel>(this.API, JSON.stringify(user), this.httpOptions).pipe(
            catchError(this.handleError)
        );
    }

    public delete(id: number): Observable<boolean> {
        return this.http.delete<boolean>(this.API + `/${id}`, this.httpOptions).pipe(
            catchError(this.handleError)
        );
    }

    public update(user: UserModel): Observable<UserModel> {
        return this.http.put<UserModel>(this.API + `/${user.id}`, JSON.stringify(user), this.httpOptions).pipe(
            catchError(this.handleError)
        );
    }

    handleError(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            errorMessage = `Error: ${error.error.message}`;
        } else {
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        return throwError(errorMessage);
    }

}
