import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { PointsLocalModel } from '../Models/Points.LocalModel';
import { RequestService } from '../Services/requests.service';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { DBTables } from '../Enums/DBTables.enums';

@Injectable({
    providedIn: 'root'
  })
export class ItemResolve implements Resolve<PointsLocalModel> {
    constructor(
        private requestService: RequestService,
        private dbService: NgxIndexedDBService
    ) { }

    resolve(route: ActivatedRouteSnapshot): PointsLocalModel | Observable<PointsLocalModel> | Promise<PointsLocalModel> {
        const id = route.paramMap.get('id');
        const point = new PointsLocalModel();
        let exitsItem = false;
        return this.dbService.getByID(DBTables.Points, route.paramMap.get('id')).then((res: PointsLocalModel) => {
            if (res !== null || res !== undefined) {
                exitsItem = true;
                return res;
            } else {
                exitsItem = false;
                point.setValuesFromObject(this.requestService.findOne(Number(id)));
                return point;
            }
        });
    }
}
