import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserModel } from '../../../Core/Models/User.model';
import { AuthService } from '../../../Core/Services/auth.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  private ngUnsubscribe = new Subject();
  LoginForm: FormGroup;
  User: UserModel;
  public baseRoute = '/Auth';

  constructor(
    private inputFB: FormBuilder,
    private authService: AuthService,
    private toastController: ToastController,
    private router: Router
  ) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.LoginForm = this.inputFB.group({
      folio: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }

  login() {
    if (this.LoginForm.status === 'VALID') {
      this.authService.login(this.getUser);
      if (this.authService.loggedIn()) {
        this.router.navigateByUrl('/List/Items');
      }
    } else {
      this.invalidToast();
    }
  }

  singup() {
    this.router.navigateByUrl('/Auth/Register');
  }

  get getUser(): UserModel {
    const provControl = this.LoginForm.controls;
    const crendentials: UserModel = new UserModel();
    crendentials.folio = provControl.folio.value;
    crendentials.password = provControl.password.value;
    return crendentials;
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  private async invalidToast() {
    const toast = await this.toastController.create({
      message: 'Fill the fields',
      duration: 2000
    });
    toast.present();
  }

}
