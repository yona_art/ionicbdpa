import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './login/login.component';
import { AppMaterialModule } from '../../app-material.module';
import { IonicModule } from '@ionic/angular';
import { MatFormFieldModule } from '@angular/material';
import { RegisterComponent } from './register/register.component';

@NgModule({
  declarations: [LoginComponent, RegisterComponent],
  imports: [
    CommonModule,
    AuthRoutingModule,
    AppMaterialModule,
    MatFormFieldModule,
    IonicModule
  ]
})
export class AuthModule { }
