import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserModel } from '../../../Core/Models/User.model';
import { Subject } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../../Core/Services/auth.service';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit, OnDestroy {
  private ngUnsubscribe = new Subject();
  RegisterForm: FormGroup;
  User: UserModel;

  constructor(
    private inputFB: FormBuilder,
    private authService: AuthService,
    private toastController: ToastController,
    private router: Router
  ) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.RegisterForm = this.inputFB.group({
      name: ['', [Validators.required]],
      folio: ['', [Validators.required]],
      password: ['', [Validators.required]],
      phone: ['', [Validators.required, Validators.maxLength(18)]],
    });
  }

  Register() {
    if (this.RegisterForm.status === 'VALID') {
      this.authService.register(this.getUser);
      if (this.authService.loggedIn()) {
        this.router.navigateByUrl('/List/Items');
      }
    } else {
      this.invalidToast();
    }
  }

  toLogin() {
    this.router.navigateByUrl('/Auth/Login');
  }


  get getUser(): UserModel {
    const provControl = this.RegisterForm.controls;
    const crendentials: UserModel = new UserModel();
    crendentials.id = null;
    crendentials.folio = provControl.folio.value;
    crendentials.name = provControl.name.value;
    crendentials.password = provControl.password.value;
    crendentials.phone = provControl.phone.value;
    return crendentials;
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  private async invalidToast() {
    const toast = await this.toastController.create({
      message: 'Fill the fields',
      duration: 2000
    });
    toast.present();
  }


}
