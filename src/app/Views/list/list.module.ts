import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListRoutingModule } from './list-routing.module';
import { IonicModule } from '@ionic/angular';
import { ListItemsComponent } from './list-items/list-items.component';
import { AppMaterialModule } from '../../app-material.module';
import { MatExpansionModule } from '@angular/material';
import { NetworkService } from '../../Core/Services/network.service';
// import { EditItemComponent } from './edit-item/edit-item.component';
import { AgmDirectionModule } from 'agm-direction';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  declarations: [ListItemsComponent],
  imports: [
    CommonModule,
    ListRoutingModule,
    IonicModule,
    AppMaterialModule,
    MatExpansionModule,
    AgmDirectionModule,
    AgmCoreModule,
  ],
  providers: [
    NetworkService,
  ]
})
export class ListModule { }
