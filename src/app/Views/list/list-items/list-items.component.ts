import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Subject } from 'rxjs';
import { ToastController, AlertController, Platform } from '@ionic/angular';
import { RequestService } from '../../../Core/Services/requests.service';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { PointsLocalModel } from '../../../Core/Models/Points.LocalModel';
import { DBTables } from '../../../Core/Enums/DBTables.enums';
import { UsersService } from '../../../Core/Services/users.service';
import { TokenStorage } from '../../../Core/Services/token-storage.service';
import { StorageEnums } from '../../../Core/Enums/Storage.enums';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Router } from '@angular/router';
import { RequestModel } from '../../../Core/Models/Requests.model';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
import { LocationLocalModel } from '../../../Core/Models/Location.LocalModel';
import { FinalizedModel } from '../../../Core/Models/Finalized.model';
import { DriverLocalModel } from '../../../Core/Models/Drivers.LocalModel';

@Component({
  selector: 'app-list-items',
  templateUrl: './list-items.component.html',
  styleUrls: ['./list-items.component.scss'],
})
export class ListItemsComponent implements OnInit, OnDestroy {
  private ngUnsubscribe = new Subject();
  public points: PointsLocalModel[] = [];
  public skeletonItems: number[] = Array(20).fill(1);
  private UserId: number;
  private startCords = {
    lat: 0,
    long: 0
  };

  constructor(
    private geolocation: Geolocation,
    public alertController: AlertController,
    private launchNavigator: LaunchNavigator,
    private cdr: ChangeDetectorRef,
    private toastController: ToastController,
    private usersService: UsersService,
    private requestService: RequestService,
    private dbService: NgxIndexedDBService,
    private tokenStorage: TokenStorage,
    private plat: Platform,
  ) { }

  ngOnInit(): void {
    this.tokenStorage.getItems(StorageEnums.UserId).subscribe(res => {
      this.UserId = res;
    });
    this.getValuesIndexed();


  }

  getValuesRequests() {
    this.dbService.clear(DBTables.Points).then(() => { });
    this.points = [];
    this.usersService.findRequestByID(this.UserId, 0).subscribe(res => {
      res.map((request, index) => {
        this.dbService.add(DBTables.Locations, this.fillLocation(request)).then(() => {
        });
        const item = new PointsLocalModel();
        item.setValuesFromObject(request.request);
        if (request.request.status === 0) {
          this.dbService.add(DBTables.Points, item).then(
            (p) => {
              this.points.push(item);
            }
          );
        }
      });
    });
  }

  getValuesIndexed() {
    this.points = [];
    this.dbService.getAll(DBTables.Points).then(
      (p) => {
        p.map((pont: PointsLocalModel, index) => {
          const item = new PointsLocalModel();
          if (pont.status === 0) {
            item.setValuesFromObject(pont);
            this.points.push(item);
          } else {
          }
        });
      },
      error => { }
    );
  }

  async onCancel(id) {
    const confirm = await this.confirmationAlert('Seguro Deseas Cancelar el servicio?');
    if (confirm) {
      this.changueStatusItem(id, 2);
    }
  }

  async onDone(id) {
    const confirm = await this.confirmationAlert('Seguro Deseas terminar?');
    if (confirm) {
      this.changueStatusItem(id, 1);
    }
  }

  changueStatusItem(id: number, status: number) {
    this.dbService.getByID(DBTables.Points, id).then(res => {
      const points = new RequestModel();
      points.setValuesFromObject(res);
      this.requestService.updateStatus(points, status).subscribe(response => {
        this.dbService.update(DBTables.Points, (new PointsLocalModel()).setValuesFromObjectReturnValue(response)).then(() => {
          this.invalidToast('Actualizado correctamente');
          this.getValuesIndexed();
          this.cdr.detectChanges();
        }).then((er) => { });
      });
    }).catch(res => {
      this.invalidToast('Error con actualizar el servicio');
    });
  }

  onMap(id) {
    if (this.plat.is('desktop')) {
      this.dbService.getByID(DBTables.Points, id).then((po: PointsLocalModel) => {
        window.open(`https://maps.google.com?q=${po.latitude},${po.longitude}`);
      });
    } else {
      this.navigate(id);
    }
  }



  navigate(id) {
    this.getCords();
    this.dbService.getByID(DBTables.Points, id).then((po: PointsLocalModel) => {
      this.launchNavigator.navigate([Number(po.latitude), Number(po.longitude)], {
        start: `${this.startCords.lat}, ${this.startCords.long}`,
      }).then(
        success => alert('Launched navigator'),
        error => alert('Error launching navigator: ' + error)
      );
    });
  }

  onSync() {
    this.getValuesRequests();
  }


  fillLocation(request: FinalizedModel) {
    const item: LocationLocalModel = new LocationLocalModel();
    item.id = request.id;
    item.description = request.description;
    item.driverid = request.user.id;
    item.pointid = request.request.id;
    return item;
  }

  private async invalidToast(Message: string) {
    const toast = await this.toastController.create({
      message: Message,
      duration: 2000
    });
    toast.present();
  }

  private async confirmationAlert(message: string): Promise<boolean> {
    let resolveFunction: (confirm: boolean) => void;
    const promise = new Promise<boolean>(resolve => {
      resolveFunction = resolve;
    });
    const alert = await this.alertController.create({
      header: 'Confirmation',
      message,
      backdropDismiss: false,
      buttons: [
        {
          text: 'No',
          handler: () => resolveFunction(false)
        },
        {
          text: 'Yes',
          handler: () => resolveFunction(true)
        }
      ]
    });
    await alert.present();
    return promise;
  }

  getCords() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.startCords.lat = resp.coords.latitude;
      this.startCords.long = resp.coords.longitude;
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
