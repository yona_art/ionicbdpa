import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListItemsComponent } from './list-items/list-items.component';
// import { EditItemComponent } from './edit-item/edit-item.component';
import { ItemResolve } from '../../Core/Guards/item.resolver';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'List'
    },
    children: [
      {
        path: '',
        redirectTo: 'Items'
      },
      {
        path: 'Items',
        component: ListItemsComponent,
      },
      // {
      //   path: 'edit/:id',
      //   component: EditItemComponent,
      //   resolve: { item: ItemResolve },
      // }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [ItemResolve]
})
export class ListRoutingModule { }
