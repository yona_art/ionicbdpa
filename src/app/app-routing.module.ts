import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'Auth',
        loadChildren: () => import('./Views/auth/auth.module').then(m => m.AuthModule)
      },
      {
        path: 'List',
        loadChildren: () => import('./Views/list/list.module').then(m => m.ListModule)
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
