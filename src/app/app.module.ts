import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from './Core/Services/auth.service';
import { UsersService } from './Core/Services/users.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxIndexedDBModule, DBConfig } from 'ngx-indexed-db';
import { DBJSON } from './Core/JSON/DB';
import { NetworkService } from './Core/Services/network.service';
import { ItemResolve } from './Core/Guards/item.resolver';
import { environment } from '../environments/environment';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';
import { RequestService } from './Core/Services/requests.service';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';

const dbConfig: DBConfig = {
  name: 'MyDb',
  version: 1,
  objectStoresMeta: DBJSON
};

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgxIndexedDBModule.forRoot(dbConfig),
    AgmCoreModule.forRoot({
      apiKey: environment.MAPS_API_KEY,
    }),
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AuthService,
    UsersService,
    NetworkService,
    ItemResolve,
    RequestService,
    LaunchNavigator,
    Geolocation,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
